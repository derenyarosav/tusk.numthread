package org.example;

import java.util.List;

public class Costumer {
    private String name ;

    private List<Action> target;

    public Costumer( String name, List<Action> target ){
        this.name = name;
        this.target = target;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Costumer{" +
                "name='" + name + '\'' +
                '}';
    }

}
