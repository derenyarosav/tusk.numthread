package org.example;

import java.util.List;

public class ExchangeThread extends Thread{
    private List<Action> actionList;
    private List<Action> costumerList;

    public ExchangeThread(List<Action> actionList, List<Action> costumerList) {
        this.actionList = actionList;
        this.costumerList = costumerList;

    }


    public void setActionList(List<Action> actionList) {
        this.actionList = actionList;
    }

    public List<Action> getActionList() {
        return actionList;
    }

    public void setCostumerList(List<Action> costumerList) {
        this.costumerList = costumerList;
    }

    public List<Action> getCostumerList() {
        return costumerList;
    }

    @Override
    public void run() {
        for (int i = 0; i <= 120; i++) {
            if (costumerList.get(0).getPrice() >= actionList.get(0).getPrice()) {

                System.out.println("Спроба купівлі акції AAPL для Alice успішна. Куплено 10 акцій.");

            } else {
                System.out.println("Спроба купівлі акції AAPL для Alice не успішна.");
            }
            if (costumerList.get(0).getPrice() >= actionList.get(1).getPrice()) {
                System.out.println("Спроба купівлі акції COKE для Alice успішна. Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Alice не успішна.");
            }
            if (costumerList.get(1).getPrice() >= actionList.get(0).getPrice()) {
                System.out.println("Спроба купівлі акції AAPL для Bob успішна. Куплено 10 акцій.");
            } else {
                System.out.println("Спроба купівлі акції AAPL для Bob не успішна.");
            }
            if (costumerList.get(1).getPrice() >= actionList.get(2).getPrice()) {
                System.out.println("Спроба купівлі акції IBM для Bob успішна. Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції IBM для Bob не успішна.");
            }
            if (costumerList.get(2).getPrice() >= actionList.get(1).getPrice()) {
                System.out.println("Спроба купівлі акції COKE для Charlie успішна. Куплено 300 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Charlie не успішна.");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }

}
