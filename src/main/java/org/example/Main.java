package org.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        List<Action> actionList = new ArrayList<>();
        List<Action> target = new ArrayList<>();
        Action actionAlice = new Action("AAPL", 10, 100);
        Action actionAlice2 = new Action("COKE", 20, 390);
        Action actionBob = new Action("AAPL", 10, 140);
        Action actionBob2 = new Action("IBM", 20, 135);
        Action actionCharlie = new Action("COKE", 300, 370);
        target.add(actionAlice);
        target.add(actionAlice2);
        target.add(actionBob);
        target.add(actionBob2);
        target.add(actionCharlie);


        List<Costumer> costumerList = new ArrayList<>();
        Costumer costumer = new Costumer("Alice", target);
        Costumer costumer1 = new Costumer("Bob", target);
        Costumer costumer2 = new Costumer("Charlie", target);
        costumerList.add(costumer);
        costumerList.add(costumer1);
        costumerList.add(costumer2);
        Action action = new Action("AAPL", 100, 141);
        Action action1 = new Action("COKE", 1000, 387);
        Action action2 = new Action("IBM", 200, 137);
        actionList.add(action);
        actionList.add(action1);
        actionList.add(action2);
        ChangePriceThread changePriceThread = new ChangePriceThread(actionList);
        ExchangeThread exchangeThread = new ExchangeThread(actionList, target);
        changePriceThread.start();
        exchangeThread.start();
        Thread.sleep(610000);
        System.out.println("The end of the exchange");
        System.out.println("The final statistic : ");
        System.out.println("Останнє значення акції AAPL " + actionList.get(actionList.size() - 3));
        System.out.println("Останнє значення акції COKE " + actionList.get(actionList.size() - 2));
        System.out.println("Останнє значення акції IBM " + actionList.get(actionList.size() - 1));

    }

}