package org.example;

import java.time.LocalDateTime;
import java.util.List;

public class ChangePriceThread extends Thread{
    List<Action> actionList;

    public ChangePriceThread(List<Action> actionList) {
        this.actionList = actionList;

    }

    public void setActionList(List<Action> actionList) {
        this.actionList = actionList;
    }

    public List<Action> getActionList() {
        return actionList;
    }


    @Override
    public void run() {
        for (int i = 0; i <= 20; i++) {
            LocalDateTime localDateTime = LocalDateTime.now();
            System.out.println(localDateTime + " Ціна акцій компанії AAPL змінилась. Поточна вартість: " + actionList.get(0).changePrice());
            System.out.println(localDateTime + " Ціна акцій компанії COKE змінилась. Поточна вартість: " + actionList.get(1).changePrice());
            System.out.println(localDateTime + " Ціна акцій компанії IBM змінилась. Поточна вартість: " + actionList.get(2).changePrice());
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }

}
