package org.example;

import java.util.Random;

public class Action {
    private String name;
    private int amount;
    private int price;
    public Action(String name, int amount, int price){
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public int changePrice(){
        int h = 3 * price / 100;
        int rand = new Random().nextInt(-h, h + 1);
        int changedPrice = rand + price;
        return changedPrice;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Action{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                '}';
    }
}
